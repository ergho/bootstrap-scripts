#!/bin/bash

# Installs basic setup
set -euox pipefail

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
packages=(
    sudo
    lsb-release
    sbctl
    intel-ucode
    base
    base-devel
    zram-generator
    #fwupd thinking bout it
    # hardware tools
    usbutils
    nvme-cli
    # System monitoring
    iotop
    htop
    procs
    lsof
    # Power management
    powertop
    power-profiles-daemon
    #Printer/DNS-SD
    avahi
    xh       # http tool cli
    step-cli # Certificates
    # Arch tools
    pacman-contrib
    reflector
    pkgfile
    # Terminal,shell & tools
    alacritty
    man-db
    man-db-pages
    git
    git-filter-repo
    neovim
    exa # better ls
    fd
    sd
    jq
    nnn #terminal file manager, evaluating for neovim
    ripgrep
    ripgrep-all
    bat
    rsync
    shellcheck
    shfmt
    curl
    renameutils
    #Rust
    rustup
    cargo-audit
    cargo-edit
    cargo-outdated
    cargo-udeps
    cargo-release
    #fonts
    noto-fonts
    noto-fonts-extra
    noto-fonts-emoji
    noto-fonts-cjk
    ttf-liberation
    ttf-caladea
    ttf-carlito
    ttf-fira-code
    ttf-fira-mono
    ttf-fira-sans
    ttf-roboto
    ttf-cascadia-code
    # Desktop
    lightdm
    awesome
    pcmanfm
    firefox
    youtube-dl
    xdg-user-dirs
    xdg-utils
    cups
    bluez
    pipewire-pulse
    arandr
    pavucontrol
)

pacman -Syu --needed "${packages[@]}"

optdeps=(
    # pipewire: zeroconf support
    pipewire-zeroconf
    # dracut: --uefi, stripping, and efi signing
    binutils elfutils sbsigntools
    # dracut: tpm2-tss
    tpm2-tools
    # libva: intel drivers
    intel-media-driver
    # aurutils: chroot support
    devtools
)

pacman -S --needed --asdeps "${optdeps[@]}"
pacman -D --asdeps "${optdeps[@]}"

# Setup regular scrubbing on btrfs
systemctl enable "btrfs-scrub@$(systemd-escape -p /).timer"
if [[ -n "${SUDO_USER:-}" ]]; then
    systemctl enable "btrfs-scrub@$(systemd-escape -p "/home/${SUDO_USER}").timer"
fi

# systemd configuration
install -Dpm644 "$DIR/etc/systemd/system-ergho.conf" /etc/systemd/system.conf.d/50-ergho.conf
# Swap on zram
install -Dpm644 "$DIR/etc/systemd/zram-generator.conf" /etc/systemd/zram-generator.conf

# Update boot loader automatically
systemctl enable systemd-boot-update.service
# homed for user management and home areas
systemctl enable systemd-homed.service
# Userspace OOM killer, more selective than kernel
install -Dpm644 "$DIR/etc/systemd/oomd-ergho.conf" /etc/systemd/oomd.conf.d/oomd-ergho.conf
install -Dpm644 "$DIR/etc/systemd/root-slice-oomd-ergho.conf" /etc/systemd/system/-.slice.d/50-oomd-ergho.conf
install -Dpm644 "$DIR/etc/systemd/user-service-oomd-ergho.conf" /etc/systemd/system/user@.service.d/50-oomd-ergho.conf
systemctl enable systemd-oomd.service

# Desktop manager
systemctl enable lightdm.service

# trim all filesystems
systemctl enable fstrim.timer
# pacman cache cleanup and file database updates
systemctl enable paccache.timer
systemctl enable pkgfile-update.timer
# periodically update mirrorlist
install -Dpm644 "$DIR/etc/reflector.conf" /etc/xdg/reflector/reflector.conf
# power management
systemctl enable power-profiles-daemon.service
# DNS resolver w caching
ln -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
install -Dpm644 "$DIR/etc/systemd/resolved-ergho.conf" /etc/systemd/resolved.conf.d/50-ergho.conf
systemctl enable systemd-resolved.service
# networking
install -Dpm644 "$DIR/etc/networkmanager-mdns.conf" /etc/NetworkManager/conf.d/50-mdns.conf
systemctl enable NetworkManager.service
systemctl enable avahi-daemon.service
# Printing and other desktop services
systemctl enable cups.service
systemctl enable bluetooth.service

# Allow myself to use rootless container
if [[ -n "${SUDO_USER:-}" ]]; then
    usermod --add-subuids 165536-231072 --add-subgids 165536-231072 "$SUDO_USER"
fi

# Configure account locking
install -pm644 "$DIR/etc/faillock.conf" /etc/security/faillock.conf

# Sudo settings
install -dm750 /etc/sudoers.d/
install -pm600 -t/etc/sudoers.d "$DIR"/etc/sudoers.d/*
# System settings and module parameters
install -pm644 "$DIR/etc/sysctl-ergho.conf" /etc/sysctl.d/90-ergho.conf
install -pm644 "$DIR/etc/modprobe-ergho.conf" /etc/modprobe.d/modprobe-ergho.conf

install -pm644 "$DIR/etc/ergho-dracut.conf" /etc/dracut.conf.d/50-ergho.conf
if [[ -f /usr/share/secureboot/keys/db/db.key ]] && [[ -f /usr/share/secureboot/keys/db/db.pem ]]; then
    install -pm644 "$DIR/etc/ergho-dracut-sbctl.conf" /etc/dracut.conf.d/90-ergho-sbctl-signing.conf
else
    rm -f /etc/dracut.conf.d/90-ergho-sbctl-signing.conf
fi

# If we have secureboot tooling in place
if command -v sbctl >/dev/null && [[ -f /usr/share/secureboot/keys/db/db.key ]]; then
    # Remove legacy signing for bootloader.
    for file in /efi/EFI/BOOT/BOOTX64.EFI /efi/EFI/systemd/systemd-bootx64.efi; do
        sbctl remove-file "$file" || true
    done

    # Generate signed bootloader image
    if ! sbctl list-files | grep -q /usr/lib/systemd/boot/efi/systemd-bootx64.efi; then
        sbctl sign -s -o /usr/lib/systemd/boot/efi/systemd-bootx64.efi.signed /usr/lib/systemd/boot/efi/systemd-bootx64.efi
        bootctl update --graceful
    fi

   # # Generate signed firmware updater
   # if ! sbctl list-files | grep -q /usr/lib/fwupd/efi/fwupdx64.efi; then
   #     sbctl sign -s -o /usr/lib/fwupd/efi/fwupdx64.efi.signed /usr/lib/fwupd/efi/fwupdx64.efi
   # fi

    # Update all secureboot signatures
    sbctl sign-all

    # Dump signing state just to be on the safe side
    sbctl verify
fi

# Install or update, and then configure the bootloader.
# Do this AFTER signing the boot loader with sbctl, see above, to make sure we
# install the signed loader.
if ! [[ -e /efi/EFI/BOOT/BOOTX64.EFI ]]; then
    bootctl install
else
    bootctl update --graceful
fi
install -pm644 "$DIR/etc/loader.conf" /efi/loader/loader.conf

