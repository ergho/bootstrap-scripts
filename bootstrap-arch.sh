#!/bin/bash

set -euox pipefail

args=()

encryption="yes"
target_device=""
new_hostname=""

while [[ $# -gt 0 ]]; do
    arg="$1"

    case $arg in
    --not-encrypted)
        encryption="no"
        shift
        ;;
    --device)
        target_device="$2"
        shift
        shift
        ;;
    --hostname)
        new_hostname="$2"
        shift
        shift
        ;;
    *)
        args+=("$arg")
        shift
        ;;
    esac
done

if [[ -z "$target_device" ]]; then
    echo "Missing --device <device> arg" >&2
    exit 2
fi

if [[ -z "$new_hostname" ]]; then
    echo "Missing --hostname <hostname> arg" >&2
    exit 2
fi

if [[ "${#args[@]}" -ne 0 ]]; then
    echo "Unexpected args: ${args[*]}" >&2
    exit 2
fi

if [[ "$UID" -ne 0 ]]; then
    echo "This script needs to be ran as root!" >&2
    exit 3
fi

read -rp "THIS SCRIPT IS DESTRUCTIVE AND WILL OVERWRITE ALL CONTENT OF ${target_device}. Type uppercase yes to continue: " confirmed

if [[ "$confirmed" != "YES" ]]; then
    echo "aborted" >&2
    exit 128
fi

# Partition
sgdisk -Z "$target_device"
sgdisk \
    -n1:0:+1G -t1:ef00 -c1:EFISYSTEM \
    -N2 -t2:8304 -c2:linux \
    "$target_device"

# Reload partition table
sleep 3
partprobe -s "$target_device"
sleep 3

# Encryption
if [[ "$encryption" == "yes" ]]; then
    cryptsetup luksFormat --type luks2 /dev/disk/by-partlabel/linux
    cryptsetup luksOpen /dev/disk/by-partlabel/linux root
    root_device="/dev/mapper/root"
else
    root_device="/dev/disk/by-partlabel/linux"
fi

# Filesystems
mkfs.fat -F32 -n EFISYSTEM /dev/disk/by-partlabel/EFISYSTEM
mkfs.btrfs -f -L linux "$root_device"

# Mount subvolume and create additional subvolumes
mount "$root_device" /mnt
mkdir /mnt/efi
for subvol in var var/log var/cache var/tmp srv home; do
    btrfs subvolume create "/mnt/$subvol"
done

# Disable CoW for /home large loopback files with systemd-homed
chattr -C /mnt/home

# Compress /usr/ to save space
mkdir -m755 /mnt/usr
btrfs prop set /mnt/usr compression zstd:1

# Mount additional partitions
mount /dev/disk/by-partlabel/EFISYSTEM /mnt/efi

# Bootstrap new chroot
reflector --save /etc/pacman.d/mirrorlist --protocol https --country Sweden --latest 5 --sort age
pacstrap /mnt base linux linux linux-firmware intel-ucode btrfs-progs dracut neovim networkmanager openssh sbsigntools sbctl sudo exfatprogs

# Configure timezone, locale, keymap
ln -sf /usr/share/zoneinfo/Europe/Stockholm /mnt/etc/localtime
sed -i \
    -e '/^#en_US.UTF-8/s/^#//' \
    /mnt/etc/locale.gen
echo 'LANG=en_US.UTF-8' >/mnt/etc/locale.conf
echo 'KEYMAP=us' >/mnt/etc/vconsole.conf

# Hostname and Network
echo "$new_hostname" >/mnt/etc/hostname
ln -sf /run/systemd/resolve/stud-resolv.conf /mnt/etc/resolv.conf

# Switch chroot
cat <<'EOF' | arch-chroot /mnt
set -euox pipefail
# generate locales
locale-gen
# Install dracut opt deps required to build unified kernel images
pacman -S --noconfirm --asdeps binutils elfutils
for kver in /lib/modules/*; do dracut -f --uefi --kver "${kver##*/}"; done
# install bootloader
bootctl install
EOF

echo "Enable resolved"
systemctl --root /mnt enable systemd-resolved
echo "Enable NetworkManager"
systemctl --root /mnt enable NetworkManager
echo "Enable sshd"
systemctl --root /mnt enable sshd
echo "Enable homed"
systemctl --root /mnt enable systemd-homed

echo "Set root pass"
passwd -R /mnt root

echo "FINISHED"
echo "Do next step after reboot"
